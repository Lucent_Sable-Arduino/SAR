#include <avr/io.h>
#include <avr/interrupt.h>
#include <lcd.h>
#include <adc.h>
#include <stdio.h>
#include <util/delay.h>

#define START 0
#define STOP  1
#define RESET 2

#define ENABLE_CLOCK 0x0A
#define DISABLE_CLOCK 0x00

volatile int millis = 0;
volatile int secs = 0;
volatile int mins = 0;
volatile int hours = 0;

volatile int mode = START;

//with the current settings
//Refrence: 07:38:499
//Timer:    07:37:39
//need to compensate for this drift
//1 second in 7:30
//this is 2.2ms per second

int main(void)
{
	lcd_init();
	serial_init();
	cli();

	/*Timer for PWM*/
	//set pin3 of port B to output
	DDRB |= 0x08;
	//set clock division and mode to /1 and PWM
	//TCCR2A = 0x83;
	TCCR2A = ((1<<COM2A1)|(1<<WGM20)|(1<<WGM21));
	//TCCR2B = 0x09;
	TCCR2B = (1<<CS20);

	//no interrupt
	TIMSK2 = 0x00;

	//initialise the comparison register
	OCR2A = 127; //50% duty

	/* Globally enable interrupts */
	sei();

	//Use a pin as a external trigger, falling edge on
	//start of conversion
	DDRB |= 0x02;
	DDRB &=~0x01;
	int adc = 0;
	int sar = 0;
	char buff[30];
	adc_init(4);
	while (1)
	{
		int i = 8;
		int j;
		/*Main loop, for now just ADC*/
		//set the DAC output to 2.5V
		OCR2A = 0x80;
		//wait for capacator to charge
		PORTB &= ~0x02;
		for(i=7;i>=0;i--)
		{
			//_delay_us(31*i);
			for(j = i; j>=0;j--)
			{
				_delay_us(30);
			}
			//check the comparison pin
			if(!(PINB & 0x01))
			{
				//if it is high, then we are over, otherwise we are under
				//if we are over, then we need to come down some
				//set the current bit to 0 and the next bit to 1
				OCR2A &= ~(1<<i);
			}
			//otherwise, just set the next bit to 1;
			OCR2A |= (1<<(i-1));
		}
		PORTB |= 0x02;
		sar = OCR2A;
		adc = adc_read();
		lcd_clear();
		printf("SAR:%03d,ADC:%04d\n", sar, adc);
		sprintf(buff, "SAR:%d\nADC:%d", sar,adc);
		lcd_display(buff);
		_delay_ms(100);
	}

	return 0;
}

